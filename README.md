# PyTorch-MNIST
Simplified adaption of [PyTorch MNIST example](https://github.com/pytorch/examples/tree/master/mnist). This repository splits the code into parts (training, model and data) for a better overview and further adaptations.

## Requirements
pip install -r requirements.txt

## Run
python main.py
